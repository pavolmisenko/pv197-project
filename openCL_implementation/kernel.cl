unsigned char transform_AA( int AA )
{
    if ( AA == 1 || AA == 2 ) return 1;
    if ( AA == 7 || AA == 12 ) return 2;
    return 0;
}

unsigned char is_bridge( unsigned char a, unsigned char b )
{
    return ( a && b ) && ( a != b );
}

__kernel void compute_chains(
    __global float* primaryScore,
    __global float* secondaryScore,
    __global const int* chains,
    __global const float* tableA,
    __global const float* tableB,
    __local int* sharedChains,
    __local unsigned char* transChains,
    __local unsigned char* future,
    __local float* primaryTable,
    __local float* secondaryTable )
{
    /* DATA PREPARATION */

    /* Primary and Secondary score. */
    float primary = 0.0f;
    float secondary = 0.0f;

    /* Indicator about salt bridg in each sequence */
    unsigned char null = 0;

    /* Work-item and Work-group specific identificators */
    size_t stride = get_global_id(0);
    size_t idX = get_local_id(0);
    size_t block = get_local_size(0);
    size_t global_size = get_global_size(0);
    //if ( stride == global_size - 1 ) printf("GLOBAL SIZE = %d\n", global_size);

    //if (idX == 0) printf("IDX %ld : GLOBAL ID - %ld | GLOBAL SIZE = %ld | LOCAL SIZE = %ld\n", idX, get_global_id(0), get_global_size(0), get_local_size(0));

    /* Copy part of chain from GLOBAL memory to SHARED memory */
    sharedChains[idX] = chains[stride];
    transChains[idX] = transform_AA( chains[stride] );
    if ( idX < 18 ) {
        sharedChains[idX + block] = chains[stride + block];
        transChains[idX + block] = transform_AA( chains[stride + block] );
    }
    /* Copy tableA and tableB from GLOBAL to SHARED memory. */
    int k = 0;
    for ( ; k < ( 20 * 18 ) / block; ++k ) {
        primaryTable[idX+ ( block * k )] = tableA[idX + ( block * k )];
        secondaryTable[idX + ( block * k )] = tableB[idX + ( block * k )];
    }
    if ( idX < ( 20 * 18 ) % block ) {
        primaryTable[idX + ( block * k )] = tableA[idX + ( block * k )];
        secondaryTable[idX + ( block * k )] = tableB[idX + ( block * k )];
    }

    barrier(CLK_LOCAL_MEM_FENCE);

    /* CALCULATION */

    /* Position 0 -> Looking for bridge on position 0 (looking forward).
       Each THREAD will check bridge on its starting position. */
    int currentAA = sharedChains[ idX ];
    
    null |= is_bridge( transChains[ idX ], transChains[ idX + 2 ] );
    null |= is_bridge( transChains[ idX ], transChains[ idX + 3 ] );
    null |= is_bridge( transChains[ idX ], transChains[ idX + 5 ] );
    
    future[ idX ] = null;
    barrier(CLK_LOCAL_MEM_FENCE);

    /* Check if next 12 threads found bridge. */                            
    if ( idX < block - 12 ) { 
        null |= future[ idX + 1 ];
        null |= future[ idX + 2 ];
        null |= future[ idX + 3 ];
        null |= future[ idX + 4 ];
        null |= future[ idX + 5 ];
        null |= future[ idX + 6 ];
        null |= future[ idX + 7 ];
        null |= future[ idX + 8 ];
        null |= future[ idX + 9 ];
        null |= future[ idX + 10 ];
        null |= future[ idX + 11 ];
        null |= future[ idX + 12 ];
    }

    /* Last 12 threads don't have enough follow up threads and need to
       calculate bridges by themselves. */
    else {
        for( int l = 1; l < 13; ++l ) {
            int idx5 = l + 5;
            int idx3 = l + 3;
            int idx2 = l + 2;

            null |= is_bridge( transChains[ idX + l ], transChains[ idX + idx2 ] );
            null |= is_bridge( transChains[ idX + l ], transChains[ idX + idx3 ] );
            null |= is_bridge( transChains[ idX + l ], transChains[ idX + idx5 ] );
        }
    }

    /* If bridge found end thread.*/
    int i = 1;
    if ( null ) {
        goto end;
    }

    primary += primaryTable[currentAA * 18]; 
    secondary += secondaryTable[currentAA * 18];
    
    /* Position 1 - 12 -> No bridge on position 0 - 12. */
    for ( ; i < 13; ++i ) {
        
        int currentAA = sharedChains[ idX + i ];

        primary += primaryTable[ currentAA * 18 + i ]; 
        secondary += secondaryTable[ currentAA * 18 + i ];

    }

    /* Position 13 - 17 -> Calculate score in overflowing regions. */
    for ( ; i < 18; ++i ) {

        int idx5 = (i < 13) ? i + 5 : i - 13;
        int idx3 = (i < 15) ? i + 3 : i - 15;
        int idx2 = (i < 16) ? i + 2 : i - 16;

        int currentAA = sharedChains[ idX + i ];

        null |= is_bridge( transChains[ idX + i ], transChains[ idX + idx2 ] );
        null |= is_bridge( transChains[ idX + i ], transChains[ idX + idx3 ] );
        null |= is_bridge( transChains[ idX + i ], transChains[ idX + idx5 ] );

        if ( null ) {
            break;
        }

        primary += primaryTable[currentAA * 18 + i]; 
        secondary += secondaryTable[currentAA * 18 + i];
    }
    /* STORE data to GLOBAL memory. */
end:
    primaryScore[stride] = (float)(1 - null) * primary;
    secondaryScore[stride] = (float)(1 - null) * secondary;
}



