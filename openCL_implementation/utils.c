#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>

void printBestSequence(float* primaryScore, float* secondaryScore, const int *chains, const int n, const float cutoff) {
    int bestIdx = 0;
    for (int i = 0; i < n; i++) {
        if ((secondaryScore[i] < cutoff) && (primaryScore[i] > primaryScore[bestIdx]))
            bestIdx = i;
    }

    const char indicesToChar[] = {'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K', 'L', 'M', 'N', 'Q', 'R', 'S', 'T', 'V', 'W', 'X', 'Y', 'Z'};
    printf("The best sequence: ");
    for (int i = bestIdx; i < bestIdx+18; i++)
        printf("%c", indicesToChar[chains[i]]);
    printf(" (primary score %f, secondary score %f)\n", primaryScore[bestIdx], secondaryScore[bestIdx]);
}

char* read_file( const char *path ) {
    FILE *fp;
    long lSize;
    char *buffer = NULL;

    fp = fopen ( path , "r" );
    if ( !fp ) {
        printf("Open file ERROR: %s\n", strerror(errno));
        return NULL;
    }

    fseek( fp, 0L, SEEK_END );
    lSize = ftell( fp ); 
    rewind( fp );

    /* Allocate memory for entire content */
    buffer = (char*) malloc( lSize + 1 );
    if( !buffer ) {
        fclose(fp);
        fputs("memory alloc fails", stderr);
        exit(1);
    }
        
    /* Copy the file into the buffer */
    if( 1 != fread( buffer , lSize , 1 , fp) ) {
        fclose(fp);
        free(buffer);
        fputs("entire read fails",stderr);
        exit(1);
    }
    buffer[lSize] = '\0';
    
    fclose(fp);
    return buffer;
}

int loadScoreTable( float table[20*18], const char *path ) {
    char *text, *old_text = NULL;
    text = read_file( path );
    old_text = text;

    if ( !text ) {
        printf("Error: Failed to open file. %s\n", strerror(errno));
        exit( EXIT_FAILURE );
    }

    char *token  = strchr(text, '\n');

    int i = 0;
    while (token != NULL)
    {
        *token++ = '\0';
        char *token2 = strtok(text, ",");
        while (token2 != NULL)
        {
            float num = atof(token2);
            table[i++] = num;
            token2 = strtok(NULL, ",");
        }
        text = token;
        token = strchr(text, '\n');
    }
    free( old_text );
    return( EXIT_SUCCESS );
}