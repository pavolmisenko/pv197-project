#include <CL/cl.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#include "utils.c"

#include "kernel_CPU.c"

#define CHAINS        (1024*1024*64)
#define CHAIN_LENGTH  (CHAINS+17)

#define GLOBAL         CHAINS
#define LOCAL          512

#define CUTOFF         10.0f

void generateRandomChains(int *chains, const int length) {
    for (int i = 0; i < length; i++)
        chains[i] = (long)rand() * 16l / RAND_MAX;
}

int main(int argc, char **argv){
    // Exit code
    int exit_code = 0;
    // Data for CPU computation
    float tableA[20*18];          // score tables (constant input)
    float tableB[20*18];          // score tables (constant input)
    int *chains = NULL;            // peptide chains (input)
    float *primaryScore = NULL;    // primary score (output)
    float *secondaryScore = NULL;  // secondary score (output)
    // HOST mirror of GPU results
    float *gpu_primaryScore = NULL;    // primary score (output)
    float *gpu_secondaryScore = NULL;  // secondary score (output)

    chains = malloc( sizeof(int) * CHAIN_LENGTH );
    generateRandomChains( chains, CHAIN_LENGTH );

    /* --- Solve CPU --- */
    primaryScore = malloc( sizeof(float) * CHAINS );
    secondaryScore = malloc( sizeof(float) * CHAINS );

    if ( loadScoreTable(tableA, "../tableA.csv") ) {
        printf("Failed to load tableA\n");
    }
    if ( loadScoreTable(tableB, "../tableB.csv") ) {
        printf("Failed to load tableB\n");
    }

    printf("Solving on CPU... \n");
    clock_t cpu_start = clock();
    solveCPU(primaryScore, secondaryScore, tableA, tableB, chains, CHAINS);
    clock_t cpu_end = clock();
    double cpu_time = (double)(cpu_end - cpu_start) / CLOCKS_PER_SEC;
    printf("CPU elapsed time is %f seconds\n", cpu_time);
    printf("CPU performance: %f megachains/s\n", (double)CHAINS/cpu_time/1e6f);
    printBestSequence(primaryScore, secondaryScore, chains, CHAINS, CUTOFF);

    /* --- Open CL device --- */

    /* initializations */
    cl_device_id device_id;
    cl_context context;
    cl_command_queue commands;
    cl_program program;
    cl_kernel kernel;

    cl_int err;

    cl_mem d_tableA = NULL;
    cl_mem d_tableB = NULL;
    cl_mem d_chains = NULL;
    cl_mem d_primaryScore = NULL;
    cl_mem d_secondaryScore = NULL;

    /* Load source kerner as string */
    char * kernelSource = NULL;
    kernelSource = read_file( "kernel.cl" );
    if ( !kernelSource ) {
        fputs("Error: Cannot read Kernel", stderr);
        exit_code = 1;
        goto cleanup;
    }

    //printf("Kernel source is:\n %s \n end \n", kernelSource);
    
    /* Get Device ID */
    err = clGetDeviceIDs(NULL, CL_DEVICE_TYPE_GPU, 1, &device_id, NULL);
    if ( err ) {
        fputs("Error: No device found!", stderr);
        exit_code = 1;
        goto cleanup;
    }

    /* Get device name */
    char name[1024];
    err = clGetDeviceInfo( device_id, CL_DEVICE_NAME, 1024, &name, NULL );
    if ( err ) {
        fputs("Error: No device name!", stderr);
        exit_code = 1;
        goto cleanup;
    }
    printf("Solving on GPU \"%s\" ... \n", name);
    
    /* Create compute context */
    context = clCreateContext(NULL, 1, &device_id, NULL, NULL, &err);
    if ( !context ) {
        fputs("Error: Cannot create context!", stderr);
        exit_code = 1;
        goto cleanup;
    }

    /* Create compute commands queue */
    commands = clCreateCommandQueueWithProperties( context, device_id, 0, &err);
    if ( !commands ) {
        fputs("Error: Cannot create command queue!", stderr);
        exit_code = 1;
        goto cleanup;
    }

    /* Create program from string */
    program = clCreateProgramWithSource(context, 1, (const char **) &kernelSource, NULL, &err);
    if ( !program ) {
        fputs("Error: Failed to create kernel program!", stderr);
        exit_code = 1;
        goto cleanup;
    }

    /* Build program executable */
    err = clBuildProgram( program, 0, NULL, NULL, NULL, NULL );
    if ( err != CL_SUCCESS )
    {
        size_t len;
        char buffer[2048];

        fputs("Error: Failed to build program!", stderr);
        clGetProgramBuildInfo( program, device_id, CL_PROGRAM_BUILD_LOG, sizeof( buffer ), buffer, &len );
        printf("%s\n", buffer);
        exit_code = 1;
        goto cleanup;
    }

    /* Create the compute kernel in the program */
    kernel = clCreateKernel( program, "compute_chains", &err );
    if ( !kernel || err != CL_SUCCESS )
    {
        fputs("Error: Failed to create compute kernel!", stderr);
        exit_code = 1;
        goto cleanup;
    }

    /* Create the input and output arrays in device memory */
    d_primaryScore = clCreateBuffer(context, CL_MEM_READ_WRITE, sizeof(float) * CHAINS, NULL, NULL); //TODO - I can already assign data to be copied here. No need for (*1 clause)
    d_secondaryScore = clCreateBuffer(context, CL_MEM_READ_WRITE, sizeof(float) * CHAINS, NULL, NULL);
    d_tableA = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(float) * 20 * 18, NULL, NULL);
    d_tableB = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(float) * 20 * 18, NULL, NULL);
    d_chains = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(int) * CHAIN_LENGTH, NULL, NULL);
    if ( !d_primaryScore || !d_secondaryScore || !d_tableA || !d_tableB || !d_chains )
    {
        fputs("Error: Failed to allocate device memory!", stderr);
        exit_code = 1;
        goto cleanup;
    }

    /* Write our data set into the input array in device memory */
    err = 0;
    err |= clEnqueueWriteBuffer(commands, d_tableA, CL_TRUE, 0, sizeof(float) * 20 * 18, tableA, 0, NULL, NULL); // (*1)
    err |= clEnqueueWriteBuffer(commands, d_tableB, CL_TRUE, 0, sizeof(float) * 20 * 18, tableB, 0, NULL, NULL);
    err |= clEnqueueWriteBuffer(commands, d_chains, CL_TRUE, 0, sizeof(int) * CHAIN_LENGTH, chains, 0, NULL, NULL);
    if (err != CL_SUCCESS)
    {
        fputs("Error: Failed to write to source array!", stderr);
        printf("\nERROR: %d\n", err);
        exit_code = 1;
        goto cleanup;
    }
    

    /* Set the arguments to our compute kernel */
    err = 0;
    err |= clSetKernelArg(kernel, 0, sizeof(cl_mem), &d_primaryScore);
    err |= clSetKernelArg(kernel, 1, sizeof(cl_mem), &d_secondaryScore);
    err |= clSetKernelArg(kernel, 2, sizeof(cl_mem), &d_chains);
    err |= clSetKernelArg(kernel, 3, sizeof(cl_mem), &d_tableA);
    err |= clSetKernelArg(kernel, 4, sizeof(cl_mem), &d_tableB);
    err |= clSetKernelArg(kernel, 5, (LOCAL + 18) * sizeof(int), NULL); //sharedChains
    err |= clSetKernelArg(kernel, 6, (LOCAL + 18) * sizeof(char), NULL); //transChains
    err |= clSetKernelArg(kernel, 7, LOCAL * sizeof(char), NULL); //future
    err |= clSetKernelArg(kernel, 8, 20 * 18 * sizeof(float), NULL); //primaryTable
    err |= clSetKernelArg(kernel, 9, 20 * 18 * sizeof(float), NULL); //secondaryTable
    if (err != CL_SUCCESS)
    {
        printf("Error: Failed to set kernel arguments! %d\n", err);
        exit_code = 1;
        goto cleanup;
    }

    /*Kernel Sizes*/
    const size_t global = GLOBAL;
    const size_t local = LOCAL;

    /* Execute kernel on input data */
    clock_t gpu_start = clock();

    err = clEnqueueNDRangeKernel(commands, kernel, 1, NULL, &global, &local, 0, NULL, NULL);
    if (err)
    {
        printf("Error: Failed to execute kernel!\n");
        exit_code = 1;
        goto cleanup;
    }

    /* Wait for the command commands to get serviced before reading back results */
    clFinish(commands);

    clock_t gpu_end = clock();
    double gpu_time = (double)(gpu_end - gpu_start) / CLOCKS_PER_SEC;
    printf("Standard: GPU elapsed time is %f seconds\n", gpu_time);
    printf("Standard: GPU performance: %f megachains/s\n", (float) CHAINS/gpu_time/1e6f);

    /* Read back the results from the device to verify the output */
    gpu_primaryScore = malloc( sizeof( float ) * CHAINS );
    gpu_secondaryScore = malloc( sizeof( float ) * CHAINS );
    err = 0;
    err |= clEnqueueReadBuffer( commands, d_primaryScore, CL_TRUE, 0, sizeof(float) * CHAINS, gpu_primaryScore, 0, NULL, NULL );  
    err |= clEnqueueReadBuffer( commands, d_secondaryScore, CL_TRUE, 0, sizeof(float) * CHAINS, gpu_secondaryScore, 0, NULL, NULL );
    if (err != CL_SUCCESS)
    {
        free(gpu_primaryScore);
        free(gpu_secondaryScore);
        printf("Error: Failed to read output array! %d\n", err);
        exit_code = 1;
        goto cleanup;
    }
    printBestSequence(gpu_primaryScore, gpu_secondaryScore, chains, CHAINS, CUTOFF);

    //const char indices_to_char[] = {'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K', 'L', 'M', 'N', 'Q', 'R', 'S', 'T', 'V', 'W', 'X', 'Y', 'Z'};
    for (int i = 0; i < CHAINS; i++) {
        if (fabsf(gpu_primaryScore[i] - primaryScore[i]) > 0.001f) {
            printf("Error detected at index %i of primaryScore: %f should be %f.\n", i, gpu_primaryScore[i], primaryScore[i]);
            exit_code = 2;
            goto cleanup; // exit after first error*/
        }
        if (fabsf(gpu_secondaryScore[i] - secondaryScore[i]) > 0.001f) {
            printf("Error detected at index %i of secondaryScore: %f should be %f.\n", i, gpu_secondaryScore[i], secondaryScore[i]);
            exit_code = 2;
            goto cleanup; // exit after first error
        }
    }
    printf("Test OK.\n");

cleanup:
    if ( d_tableA ) clReleaseMemObject(d_tableA);
    if ( d_tableB ) clReleaseMemObject(d_tableB);
    if ( d_chains ) clReleaseMemObject(d_chains);
    if ( d_primaryScore ) clReleaseMemObject(d_primaryScore);
    if ( d_secondaryScore ) clReleaseMemObject(d_secondaryScore);
    clReleaseProgram(program);
    clReleaseKernel(kernel);
    clReleaseCommandQueue(commands);
    clReleaseContext(context);

    if ( kernelSource ) free(kernelSource);
    if ( gpu_primaryScore ) free(gpu_primaryScore);
    if ( gpu_secondaryScore )free(gpu_secondaryScore);
    if ( primaryScore ) free(primaryScore);
    if ( secondaryScore ) free(secondaryScore);
    if ( chains ) free(chains);
    return exit_code;
}