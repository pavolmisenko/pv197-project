// naive CPU implementation
#include <stdbool.h>
#include <stdio.h>

// 7 = K, 12 = R, 1 = D, 2 = E
bool isSaltBridge(const int a1, const int a2) {
    switch(a1) {
        case 7:
            if ((a2 == 1) || (a2 == 2))
                return true;
            break;
        case 12:
            if ((a2 == 1) || (a2 == 2))
                return true;
            break;
        case 1:
            if ((a2 == 7) || (a2 == 12))
                return true;
            break;
        case 2:
            if ((a2 == 7) || (a2 == 12))
                return true;
            break;
        default:
            break;
    }
    return false;
}

void solveCPU(float* primaryScore, float* secondaryScore, float tableA[20*18], float tableB[20*18], const int *chains, int n) {

    // iterate over sequences
    for (int i = 0; i < n; i++) {
        primaryScore[i] = 0.0f;
        secondaryScore[i] = 0.0f;
        // iterate over structure
        for (int j = 0; j < 18; j++) {
            //printf("%i ", chains[i+j]);
            primaryScore[i] += tableA[chains[i+j]*18 + j];
            secondaryScore[i] += tableB[chains[i+j]*18 + j];
            //if ( i == 8 ) {
                //printf("CPU : Pep = %d; primScore = %f; secScore %f\n", chains[i+j], tableA[chains[i+j]*18 + j], tableB[chains[i+j]*18 + j]);
                //printf("CPU : cumPrimScore = %f; cumSecScore = %f\n", primaryScore[i], secondaryScore[i]);
            //}
            int idx5 = (j > 4) ? j-5 : 18 - (5-j);
            int idx3 = (j > 2) ? j-3 : 18 - (3-j);
            int idx2 = (j > 1) ? j-2 : 18 - (2-j);
            if ((isSaltBridge(chains[idx5+i], chains[j+i]))
            || (isSaltBridge(chains[idx3+i], chains[j+i]))
            || (isSaltBridge(chains[idx2+i], chains[j+i]))) {
                primaryScore[i] = 0.0f;
                secondaryScore[i] = 0.0f;
                break;
            }
        }
        //printf("%f %f\n", primaryScore[i], secondaryScore[i]);
    }
}

void solveCPUdummy(float* primaryScore, float* secondaryScore, float tableA[20*18], float tableB[20*18], const int *chains, int n) {
    // iterate over sequences
    // iterate over sequences
    for (int i = 0; i < n; i++) {
        primaryScore[i] = 0.0f;
        secondaryScore[i] = 0.0f;
        // iterate over structure
        for (int j = 0; j < 18; j++) {
            //printf("%i ", chains[i+j]);
            primaryScore[i] += tableA[chains[i+j]*18 + j];
            secondaryScore[i] += tableB[chains[i+j]*18 + j];
            //if ( i == 8 ) {
                //printf("CPU : Pep = %d; primScore = %f; secScore %f\n", chains[i+j], tableA[chains[i+j]*18 + j], tableB[chains[i+j]*18 + j]);
                //printf("CPU : cumPrimScore = %f; cumSecScore = %f\n", primaryScore[i], secondaryScore[i]);
            //}
        }
        //printf("%f %f\n", primaryScore[i], secondaryScore[i]);
    }
}

