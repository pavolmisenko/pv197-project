# PV197 project

We will implement a tool for searching novel antibiotics based on peptides
Goal is to find a polypeptide chain with high affinity to bacterial membrane and low affinity to human membrane

Peptides are encoded as strings of amino-acids. Each peptide is represented as polypeptide chain of 18 repeating amino acids.

Peptide alphabet:
 'C', ’D’, ’E’, ’F’, ’G’, ’H’, ’I’, ’K’, ’L’, ’M’, ’N’, ’Q’, ’R’, ’S’, ’T’, ’V’, ’W’, ’X’, ’Y’, ’Z’
encoded as integers ('C' = 0, 'D' = 1, ...)

We have access to tables of affinity scores for each combination of amino-acid and position(tableA.csv and tableB.csv). 
There are some combinations that are not allowed.
Not allowed combinations are: K-D, K-E, R-D, R-E at positions ±2, ±3 and ±5. Such peptide will allways have score 0.

## What to do
We will get a string of n + 17 amino acids scored as n peptide chains (with stride 1)
We expect output as 2 arrays (primaryScore and secondaryScore) with length n. Where at each position is score for 18 amino0acid polychain.

For example if n = 3:

11  18  14   1   5  10   7   9   6   4   3  13  15  19   0  16   8  12  17   2

the sequence consists of 3 possible poly chains:
11  18  14   1   5  10   7   9   6   4   3  13  15  19   0  16   8  12

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;18  14   1   5  10   7   9   6   4   3  13  15  19   0  16   8  12  17

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;14   1   5  10   7   9   6   4   3  13  15  19   0  16   8  12  17   2


## CUDA_implementation
Contains solution using CUDA Toolkit.
Compile with:
``` nvcc -O3 -o framework framework.cu ```

## OpenCL implementation
Contains solution using OpenCL.
Compile with (Linux):
``` gcc -O3 -o framework framework.c -lOpenCL ```

MacOS: 
``` gcc -O3 -o framework framework.c -framework OpenCL ```