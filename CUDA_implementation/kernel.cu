#define BLOCK 256
__device__ unsigned char transform_AA( int AA )
{
    if ( AA == 1 || AA == 2 ) return 1;
    if ( AA == 7 || AA == 12 ) return 2;
    return 0;
}

__device__ unsigned char is_bridge( unsigned char a, unsigned char b )
{
    return ( a && b ) && ( a != b );
}

__global__ void solve( float *primaryScore, float *secondaryScore, const float *tableA,
                       const float *tableB, const int *chains )
{
    /* DATA PREPARATION */

    /* Primary and Secondary score. */
    float primary = 0.0f;
    float secondary = 0.0f;

    /* Indicator about salt bridg in each sequence */
    unsigned char null = 0;

    /* Information about bridge on position 0 in chain in each thread. */
    __shared__ unsigned char future[ BLOCK ];

    /* Absolute position of thread in GLOBAL chain. */
    int stride = blockDim.x * blockIdx.x + threadIdx.x;

    __shared__ float primaryTable[20 * 18];
    __shared__ float secondaryTable[20 * 18];
    __shared__ int shared_chains[BLOCK + 18];
    __shared__ unsigned char transf_chains[BLOCK + 18];

    /* Copy part of chain from GLOBAL memory to SHARED memory */
    shared_chains[threadIdx.x] = chains[stride];
    transf_chains[threadIdx.x] = transform_AA( chains[stride] );
    if ( threadIdx.x < 18 ) {
        shared_chains[threadIdx.x + BLOCK] = chains[stride + BLOCK];
        transf_chains[threadIdx.x + BLOCK] = transform_AA( chains[stride + BLOCK] );
    }
    /* Copy tableA and tableB from GLOBAL to SHARED memory. */
    int k = 0;
    for ( ; k < ( 20 * 18 ) / BLOCK; ++k ) {
        primaryTable[threadIdx.x + ( BLOCK * k )] = tableA[threadIdx.x + ( BLOCK * k )];
        secondaryTable[threadIdx.x + ( BLOCK * k )] = tableB[threadIdx.x + ( BLOCK * k )];
    }
    if ( threadIdx.x < ( 20 * 18 ) % BLOCK ) {
        primaryTable[threadIdx.x + ( BLOCK * k )] = tableA[threadIdx.x + ( BLOCK * k )];
        secondaryTable[threadIdx.x + ( BLOCK * k )] = tableB[threadIdx.x + ( BLOCK * k )];
    }

    __syncthreads();

    /* CALCULATION */

    /* Position 0 -> Looking for bridge on position 0 (looking forward).
       Each THREAD will check bridge on its starting position. */
    int currentAA = shared_chains[ threadIdx.x ];
    
    null |= is_bridge( transf_chains[ threadIdx.x ], transf_chains[ threadIdx.x + 2 ] );
    null |= is_bridge( transf_chains[ threadIdx.x ], transf_chains[ threadIdx.x + 3 ] );
    null |= is_bridge( transf_chains[ threadIdx.x ], transf_chains[ threadIdx.x + 5 ] );
    
    future[ threadIdx.x ] = null;
    __syncthreads();

    /* Check if next 12 threads found bridge. */                            
    if ( threadIdx.x < BLOCK - 12 ) { 
        null |= future[ threadIdx.x + 1 ];
        null |= future[ threadIdx.x + 2 ];
        null |= future[ threadIdx.x + 3 ];
        null |= future[ threadIdx.x + 4 ];
        null |= future[ threadIdx.x + 5 ];
        null |= future[ threadIdx.x + 6 ];
        null |= future[ threadIdx.x + 7 ];
        null |= future[ threadIdx.x + 8 ];
        null |= future[ threadIdx.x + 9 ];
        null |= future[ threadIdx.x + 10 ];
        null |= future[ threadIdx.x + 11 ];
        null |= future[ threadIdx.x + 12 ];
    }
    /* Last 12 threads don't have enough follow up threads and need to
       calculate bridges by themselves. */
    else {
        for( int l = 1; l < 13; ++l ) {
            int idx5 = l + 5;
            int idx3 = l + 3;
            int idx2 = l + 2;

            null |= is_bridge( transf_chains[ threadIdx.x + l ], transf_chains[ threadIdx.x + idx2 ] );
            null |= is_bridge( transf_chains[ threadIdx.x + l ], transf_chains[ threadIdx.x + idx3 ] );
            null |= is_bridge( transf_chains[ threadIdx.x + l ], transf_chains[ threadIdx.x + idx5 ] );
        }
    }

    /* If bridge found end thread.*/
    int i = 1;
    if ( null ) {
        goto end;
    }

    primary += primaryTable[currentAA * 18]; 
    secondary += secondaryTable[currentAA * 18];
    
    /* Position 1 - 12 -> No bridge on position 0 - 12. */
    for ( ; i < 13; ++i ) {
        
        int currentAA = shared_chains[ threadIdx.x + i ];

        primary += primaryTable[ currentAA * 18 + i ]; 
        secondary += secondaryTable[ currentAA * 18 + i ];

    }

    /* Position 13 - 17 -> Calculate score in overflowing regions. */
    for ( ; i < 18; ++i ) {

        int idx5 = (i < 13) ? i + 5 : i - 13;
        int idx3 = (i < 15) ? i + 3 : i - 15;
        int idx2 = (i < 16) ? i + 2 : i - 16;

        int currentAA = shared_chains[ threadIdx.x + i ];

        null |= is_bridge( transf_chains[ threadIdx.x + i ], transf_chains[ threadIdx.x + idx2 ] );
        null |= is_bridge( transf_chains[ threadIdx.x + i ], transf_chains[ threadIdx.x + idx3 ] );
        null |= is_bridge( transf_chains[ threadIdx.x + i ], transf_chains[ threadIdx.x + idx5 ] );

        if ( null ) {
            break;
        }

        primary += primaryTable[currentAA * 18 + i]; 
        secondary += secondaryTable[currentAA * 18 + i];
    }
    /* STORE data to GLOBAL memory. */
end:
    primaryScore[stride] = (float)(1 - null) * primary;
    secondaryScore[stride] = (float)(1 - null) * secondary;
}



void solveGPU(float *primaryScore, float *secondaryScore, const float *tableA, const float *tableB, const int *chains, int n) {
    dim3 dimBlock(BLOCK);
    dim3 dimGrid(n/BLOCK);
    solve<<<dimGrid, dimBlock>>>(primaryScore, secondaryScore, tableA, tableB, chains);
}