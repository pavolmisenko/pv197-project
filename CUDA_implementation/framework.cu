#include <stdlib.h>
#include <stdio.h>
#include <curand.h>
#include <string>

#include "utils.C"
#include "kernel.cu"
#include "kernel_CPU.C"

#define CHAINS        (1024*1024*32)
#define CHAIN_LENGTH  (CHAINS+17)

#define CUTOFF 10.0f

void generateRandomChains(int *chains, const int length) {
    for (int i = 0; i < length; i++)
        chains[i] = long(rand()) * 16l / RAND_MAX;
}

int main(int argc, char **argv){
    // Data for CPU computation
    float tableA[20*18];          // score tables (constant input)
    float tableB[20*18];          // score tables (constant input)
    int *chains = NULL;            // peptide chains (input)
    float *primaryScore = NULL;    // primary score (output)
    float *secondaryScore = NULL;  // secondary score (output)
    // Data for GPU computation
    float *d_tableA, *d_tableB;      // score tables (constant input)
    int *d_chains = NULL;            // peptide chains (input)
    float *d_primaryScore = NULL;    // primary score (output)
    float *d_secondaryScore = NULL;  // secondary score (output)
    // CPU mirror of GPU results
    float *gpu_primaryScore = NULL;    // primary score (output)
    float *gpu_secondaryScore = NULL;  // secondary score (output)

    // parse command line
    int device = 0;
    if (argc == 2) 
        device = atoi(argv[1]);
    if (cudaSetDevice(device) != cudaSuccess){
        fprintf(stderr, "Cannot set CUDA device!\n");
        exit(1);
    }
    cudaDeviceProp deviceProp;
    cudaGetDeviceProperties(&deviceProp, device);
    printf("Using device %d: \"%s\"\n", device, deviceProp.name);

    // create events for timing
    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    // allocate and set host memory
    chains = (int*)malloc(CHAIN_LENGTH*sizeof(chains[0]));
    primaryScore = (float*)malloc(CHAINS*sizeof(primaryScore[0]));
    secondaryScore = (float*)malloc(CHAINS*sizeof(secondaryScore[0]));
    generateRandomChains(chains, CHAIN_LENGTH);
    gpu_primaryScore = (float*)malloc(CHAINS*sizeof(primaryScore[0]));
    gpu_secondaryScore = (float*)malloc(CHAINS*sizeof(secondaryScore[0]));
 
    // allocate and set device memory
    if (cudaMalloc((void**)&d_tableA, 20*18*sizeof(d_tableA[0])) != cudaSuccess 
    || cudaMalloc((void**)&d_tableB, 20*18*sizeof(d_tableB[0])) != cudaSuccess
    || cudaMalloc((void**)&d_chains, CHAIN_LENGTH*sizeof(d_chains[0])) != cudaSuccess
    || cudaMalloc((void**)&d_primaryScore, CHAINS*sizeof(d_primaryScore[0])) != cudaSuccess
    || cudaMalloc((void**)&d_secondaryScore, CHAINS*sizeof(d_secondaryScore[0])) != cudaSuccess) {
        fprintf(stderr, "Device memory allocation error!\n");
        goto cleanup;
    }

    // clean output arrays
    cudaMemset(d_primaryScore, 0, CHAINS*sizeof(d_primaryScore[0]));
    cudaMemset(d_secondaryScore, 0, CHAINS*sizeof(d_secondaryScore[0]));

    // load score tables
    if (!loadScoreTable(tableA, "../tableA.csv") || !loadScoreTable(tableB, "../tableB.csv")) 
        goto cleanup;

    // solve on CPU
    printf("Solving on CPU...\n");
    cudaEventRecord(start, 0);
    solveCPU(primaryScore, secondaryScore, tableA, tableB, chains, CHAINS);
    cudaEventRecord(stop, 0);
    cudaEventSynchronize(stop);
    float time;
    cudaEventElapsedTime(&time, start, stop);
    printf("CPU time: %f seconds\n", time/1e3f);
    printf("CPU performance: %f megachains/s\n",
        float(CHAINS)/time/1e3f);

    printBestSequence(primaryScore, secondaryScore, chains, CHAINS, CUTOFF);

    // copy data to GPU
    cudaMemcpy(d_tableA, tableA, 20*18*sizeof(d_tableA[0]), cudaMemcpyHostToDevice);
    cudaMemcpy(d_tableB, tableB, 20*18*sizeof(d_tableB[0]), cudaMemcpyHostToDevice);
    cudaMemcpy(d_chains, chains, CHAIN_LENGTH*sizeof(d_chains[0]), cudaMemcpyHostToDevice);

    // solve on GPU
    printf("Solving on GPU...\n");
    cudaEventRecord(start, 0);
    solveGPU(d_primaryScore, d_secondaryScore, d_tableA, d_tableB, d_chains, CHAINS);
    cudaEventRecord(stop, 0);
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&time, start, stop);
    printf("GPU time: %f seconds\n", time/1e3f);
    printf("GPU performance: %f megachains/s\n",
        float(CHAINS)/time/1e3f);

    // check GPU results
    cudaMemcpy(gpu_primaryScore, d_primaryScore, CHAINS*sizeof(d_primaryScore[0]), cudaMemcpyDeviceToHost);
    cudaMemcpy(gpu_secondaryScore, d_secondaryScore, CHAINS*sizeof(d_secondaryScore[0]), cudaMemcpyDeviceToHost);

    for (int i = 0; i < CHAINS; i++) {
        if (fabsf(gpu_primaryScore[i] - primaryScore[i]) > 0.001f) {
            printf("Error detected at index %i of primaryScore: %f should be %f.\n", i, gpu_primaryScore[i], primaryScore[i]);
            goto cleanup; // exit after first error
        }
        if (fabsf(gpu_secondaryScore[i] - secondaryScore[i]) > 0.001f) {
            printf("Error detected at index %i of secondaryScore: %f should be %f.\n", i, gpu_secondaryScore[i], secondaryScore[i]);
            goto cleanup; // exit after first error
        }
    }

    printf("Test OK.\n");

cleanup:
    cudaEventDestroy(start);
    cudaEventDestroy(stop);

    if (d_chains) cudaFree(d_chains);
    if (d_primaryScore) cudaFree(d_primaryScore);
    if (d_secondaryScore) cudaFree(d_secondaryScore);
    if (d_tableA) cudaFree(d_tableA);
    if (d_tableB) cudaFree(d_tableB);

    if (chains) free(chains);
    if (primaryScore) free(primaryScore);
    if (secondaryScore) free(secondaryScore);

    if (gpu_primaryScore) free(gpu_primaryScore);
    if (gpu_secondaryScore) free(gpu_secondaryScore);

    return 0;
}
